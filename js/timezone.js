const timeZone = "-08:00";

export const toLocalTime = (hour, minute = 0) => {
    const today = new Date();
    const referenceDate = new Date(
        `${today.toDateString()} ${hour}:${minute}:00 GMT${timeZone}`
    );
    const lhour = referenceDate.getHours();
    const lminute = referenceDate.getMinutes();
    return [lhour, lminute];
};
